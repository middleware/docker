[description]
Enables a TLS(SSL) Connector on the server.
This may be used for HTTPS and/or HTTP2 by enabling
the associated support modules.

[tags]
connector
ssl

[depend]
server

[xml]
etc/jetty-ssl.xml

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
  <link rel="icon" type="image/png" href="favicon.png" />
  <link rel="stylesheet" href="/vt.css" type="text/css" media="screen" />
  <title><%= System.getProperty("jetty.hostname") %></title>
</head>
<body>
<div id="page-wrapper">
<div id="page-header"><%= System.getProperty("jetty.hostname") %></div>
<div id="content">

<h1>Unauthorized Access is Prohibited</h1>
<p>This service is covered by the <a href="http://www.policies.vt.edu/acceptableuse.php">Virginia Tech Acceptable Use</a> policy.</p>
<p><a href="/status">Server Status</a></p>
</div>

</div>
</body>
</html>
